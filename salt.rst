salt
====

Concepts
--------

* Work with Yaml, yaml + jinja, but can also support json
* Minions listen to master on port 4505 for events (or direct commands) and react accordingly (if targeted by the event), and also send data back to the master on port 4506 (tcp). They also use 4506 to request files and pillars from the server.
* You can use execution modules (from command line) or preferably salt states (infrastructure as code) to run code on minions.
* Salt connections are always initiated from the minions (no incoming trafic to open on a stateful firewall in front of the minions)
* You should see executions modules as "imperative configuration management" (you told the systems what to do to reach the result) and states as "declarative configuration management" where you told the systems in what state they should be.
* You can make your own states in python
* States are indempotent, execution modules are not. States are enforcing a policy (run code against minion every X minutes)
* Grains are grains of information, facts about the systems (kernel, os, ip, location ...). They are not case sensitive. They are refreshed every time a state is executed (highstating). You can compare them to ansible facts.
* You can use grains to target systems.
* You can create custom grains.
* Pillars are user-specific variables.
* You can use them for storing path, key/values, configuration parameters, vaulted secrets ...
* They are store on the master and only readable by the concerned minions (which the master choose).
* You should tune your master to prevent 'thundering herd' (too many minions connecting to the master at the same time, creating a bottleneck), or add other masters (multi masters)
* Multi masters can garanty HA.
* Multi masters can be set up in "redundant mode" (all minions will connect to both, so they need to share the same public key, configurations by git ...) or in "failover mode" with just one master active at a time.
* By setting file_client to "local", you can have masterless minions, they will execute salt states and commands locally.

Master bootstrap installation
-----------------------------

.. code-block:: bash

  curl -L https://bootstrap.saltstack.com -o bootstrap-salt.sh
  sudo sh bootstrap-salt.sh -P -M  # M for Master, P for Pip usage
  sudo systemctl status salt-master
  sudo journalctl -u salt-master.service

Minions bootstrap installation
------------------------------

.. code-block:: bash

  curl -L https://bootstrap.saltstack.com -o bootstrap-salt.sh
  sudo sh bootstrap-salt.sh -P # without M, P for Pip usage
  sudo systemctl status salt-minion
  sudo journalctl -u salt-minion.service

* Set your master dns or ip and restart

.. code-block:: bash

  sudo grep "master:" /etc/salt/minion
  master: 172.31.117.232
  sudo systemctl restart salt-minion

* If you want to test "masterless", you have to stop the salt-minion service, and put "local" instead of "remote" in minion config file, as well as the file_root.

Keys management
---------------

* On the master:

.. code-block:: bash

  sudo salt-key -F # print all finger keys (including master)
  sudo salt-key -L # list all keys (waiting and accepted)
  sudo salt-key -A # accept all waiting keys
  sudo salt-key -R # reject all waiting keys

* On minions, you should add the master public key in /etc/salt/minions:

.. code-block:: bash

  sudo grep  master_finger /etc/salt/minion
  master_finger: 'f3:90:bc:5f:3c:11:8c:37:58:e6:5c:02:0c:4d:9d:24:58:23:ed:1d:52:1e:a1:5e:16:09:22:f8:ef:9e:af:17'
  sudo systemctl restart salt-minion

* Show a local key on Minions:

.. code-block:: bash

  sudo salt-call --local finger.key

* Rename a minion:

.. code-block:: bash

  vi /etc/salt/minion/minion_id # give a new name
  rm /etc/salt/pki/minion/minion.p*
  sudo systemctl restart salt-minion

* On the master you can trash all previous keys or one in particular, then accept the news ones

.. code-block:: bash

  sudo salt-keys -D # delete all
  sudo salt-keys -d qsdqsdjlkj # delete a specific one

Execution modules
-----------------

.. code-block:: bash

  salt '<target>' '<function>' [arguments]
  salt '*' test.ping
  salt '*' cmd.run "whoami" runas=user
  salt 'minion1' test.ping,test.echo ,"Hello" # you can run multiple commands at once. In this example, test.ping has no arguments so we leave a blank space.

* Advance targeting: you can use grains, pillars, regex, compounds (several targetings combined) or nodegroups for targeting.

.. code-block:: bash

  salt -G "os:centos" user.add user1 home=/home/user1 # G for Grain
  salt -C "G@os:Centos or dbserver*" test.ping # C for Compound

* For nodegroups documentation: https://docs.saltstack.com/en/latest/topics/targeting/nodegroups.html
* Nodegroups should be declared in /etc/salt/master, but should be dynamically declared with compounds, not statics.
* For compounds matching see: https://docs.saltstack.com/en/latest/topics/targeting/compound.html#targeting-compound

* Complete list of execution modules: https://docs.saltstack.com/en/latest/ref/modules/all/index.html

.. code-block:: bash

  salt 'salt' sys.doc user # give informations about all user module functions
  salt 'salt' sys.doc user.add # give a resume of a specific function
  salt 'salt' sys.list_functions # same as previously, but just a list
  salt 'salt' sys.list_modules # list all modules on master

.. code-block:: bash

  salt 'minion*' pkg.install vim
  salt 'minion*' pkg.list_pkgs

* salt-call can be used to troubleshoot locally a minion (execute commands or states)

Grains
------

.. code-block:: bash

  salt 'salt' sys.list_functions grains
  salt 'salt' grains.set 'role' saltmaster # set the grain role to value saltmaster
  salt 'minion1' grains.item role # show the value of the grain role
  salt 'minion1' grains.items # show values of all grains
  salt 'minion1' grains.ls # show a list of all available grains (without their values)
  salt -G 'roles:elasticsearch' test.ping

States
------

* You need to specify on the master the file roots (where your states will be):

.. code-block:: yaml

  cat /etc/salt/master
  file_roots:
    base:
      - /srv/salt

* state example:

.. code-block:: yaml

  dana:  # reference id for the state, the name key is implicit here
    user.present:  # state function
      - name: dana  # the start of a list of values for this function
      - home: /home/dana
      - uid: 2015
      - shell: /bin/bash

* a formula is a group of states.
* to have information on states functions (which are different from remote execution functions), you can type:

.. code-block:: bash

  salt 'salt-master' sys.state_doc pkg # print the full doc of state function pkg
  salt 'salt-master' sys.list_state_functions pkg # show the available functions for the state function pkg
  salt 'salt-master' sys.state_argspec pkg.installed # show the args available for pkg.installed state function

* Basic states:

.. code-block:: bash

  vi /srv/salt/php/init.sls # init.sls is always read

  php_install:
    pkg.installed:
      - name: php

* Apply the state:

.. code-block:: bash

  salt 'minion*' state.sls php # play only the init.sls file
  salt 'minion*' state.apply php test=true # dry-run

* Include states:

.. code-block:: bash

  vi /srv/salt/php/mod-mysql.sls

  include: 
    - php
  mod_mysql_install:
    pkg.installed:
      - name: php-mysql
  salt 'minion*' state.apply php

.. code-block:: bash

  salt 'minion*' state.apply php, php.mod-mysql test=true

top.sls
-------

* the top.sls file should be at the base of your file_root directory (/srv/salt by default)
* the top.sls file defines the mappings between formulas and targets

.. code-block:: bash

  cat /srv/salt/top.sls

  base: # define base environment
    'web*': # define targets
      - php # define php formula
      - rsyslog # define rsyslog formula

* a top.sls is structured as this: environment => targets => formulas. This structure is called the state tree.

* How to for a dry run of the top.sls:

.. code-block:: bash

 salt 'minion*' state.highstate test=true
 salt 'minion*' state.show_highstate # show only the environement and the states which could be applied from the top.sls

* You can also use state.apply to run the top.sls file.

* When you include a state file in another state file, within a role, you need to specifiy "role.file". For example:

.. code-block:: bash

  cat init.sls
  apache_install:
    pkg.installed:
      - name: apache2

  apache_service:
    service.running:
      - name: apache2
      - enable: true

  include:
    - apache.config

  cat config.sls
  apache_configfile:
  file.managed:
    - name: /etc/apache2/apache2.conf
    - source: salt://apache/files/apache2.conf
    - require:
      - pkg: apache2

Requisites
----------

* List of requisites: https://docs.saltstack.com/en/latest/ref/states/requisites.html

Templating
----------

* You can use Jinja for multiple reasons: templating files, if/else conditions, macros (not repeting code), filter by, calling execution modules ...
* Jinja is always interpreted before YAML.
* You can detect Jinja code by the presence of {% %} or {{ }}
* If/else example:

.. code-block:: yaml

  apache_install:
  pkg.installed:
    {% if grains['os_family'] == 'Debian' %}
    - name: apache2
    {% elif grains['os_family'] == 'RedHat' %}
    - name: httpd
    {% endif %}

* Function calling example:

.. code-block:: yaml

  {% set apache = salt['grains.filter_by']({
  'Debian': { 'package': 'apache2' },
  'RedHat': { 'package': 'httpd' },
  }) %}

  apache_install:
    pkg.installed:
      - name: {{ apache.package }}

* Note that by default the grains.filter_by function return the os_family of the target. It returns a list.

* You should use map.jinja for this type of cases where you need variables that are no specific to your platform, but need to be known across all your formula. For example:

.. code-block:: yaml

  {% set apache = salt['grains.filter_by']({
  'Debian': { 
    'package': 'apache2',
    'service': 'apache2',
    'conf_dest': '/etc/apache2/apache2.conf',
    'conf_source': 'salt://apache/files/apache2.conf',
   },
   'RedHat': { 
     'package': 'httpd',
     'service': 'httpd',
     'conf_dest': '/etc/httpd/conf/httpd.conf',
     'conf_source': 'salt://apache/files/httpd.conf',
   }
  }) %}

* And to load the map in your states:

.. code-block:: yaml

  {% from "apache/map.jinja" import apache with context %}

  apache_configuration:
    file.managed:
      - name: {{ apache.conf_dest }}
      - source: {{ apache.conf_source }}
      - require:
        - pkg: {{ apache.package }}

Pillars
-------

* Pillars are stored in /srv/pillar (default pillars root)
* They also need a top.sls file to target minions, with a 'base' declaration.

.. code-block:: yaml

  cat /srv/pillar/top.sls

  base:
    '*':
      - elasticsearch
    'minion2':
      - mysql

* To call it directly in a state file:

.. code-block:: yaml

  [mysqld]
  bind-address = {{ pillar['mysql']['server']['bind'] }}

* But you should merge your pillars in your local variables with map.jinja.
* To list all pillars:

.. code-block:: bash

  salt '*' pillar.items

* You can use GPG on master to encrypt pillars:

.. code-block:: bash

  mkdir -p /etc/salt/gpgkeys
  chmod 0700 /etc/salt/gpgkeys
  apt-get install rng-tools
  rngd -r /dev/urandom
  gpg --gen-key --homedir /etc/salt/gpgkeys # use 'saltstack' as name
  gpg --homedir /etc/salt/gpgkeys --armor --export saltstack > exported_key.gpg
  gpg --import exported_key.gpg
  echo -n 'monpass' | gpg --armor --batch --trust-model always --encrypt -r saltstack

* Now you can use your encrypted password in pillars

.. code-block:: bash

  cat /srv/pillar/mysql.sls
  #!yaml|gpg

  mysql:
    server:
      bind: 172.31.126.111
    root:
      password: |
        -----BEGIN PGP MESSAGE-----
        hQGMAzeX3ovUdUL3AQv9HD+ygP35UUer6kLAiVB2DXtUBmEIgRAjXjBv1cv6VJOE
        wzI7SZlycp0YD7yQ+VoGeVOg7n951Nzp1njNOQWr9xa/avvCfDuvDWHwgC7dRygO
        Z/hZ4AxzOyTvQMKH0O1yNCLECL2y/7opoBiXJUiyYrzZuGMQKaRMoak2hzg3O92r
        ucAYQbtvwn+MWht+Y3CSHLy2I9ur3ceSI0erXDsRjA5s5oxDJGUO95kif3m3TAY+
        UDXjYwJg7PS3EmglUQOj7XJDd8u6HUMnTDlUblz9JhI9FTVeJAV7C1dXCGMALX5A
        dd6Oba6O3AcnwwVq4Lmzk0sw7lp3dMmgfZlfqJR/CRhSZgu5RNP5g273Cd+fnfHc
        fnipx1VRPq/QDboHnUBYDG/vflJFd7JN4GdXBVb/T5Ik8kJ2t/IBExp4Hfwttwre
        O6dpywUunBzgFZ/EiHu4W5NevNkxXMXFpKAYAzxKyq5ouWhHFE9KPZvaQeTS03c2
        uw79ESKEw7SHer6OPKXB0kIBZxZjAdgEsjqnOnpsEQCHOSNMOsSu68c+1khtiMi2
        jY0aZEw78u69KMV9/bjC8+jyQmZQ9W/xl9Otgx5rgAhNI14=
        =9tAO
        -----END PGP MESSAGE-----

.. code-block:: bash

  root_user:
    mysql_user.present:
      - name: root
      - password: {{ pillar['mysql']['root']['password'] }}
      - host: localhost

Best Practices
--------------

* You shoud use minion.d and master.d to add configuration, because otherwise a salt package update will break your configuration.
* You shoud name your jobs inside formula with "formulaname_jobname"
* When you use "require", you should specify the function before the job id for faster execution (exemple: - pkg: myjobid)
* Your states name in a formula should be clear on what they do
* You should merge your pillars as local variables, thanks to map.jinja, and put your defaults in a defaults.yml file.

.. code-block:: bash

  {% import_yaml 'elasticsearch/defaults.yml' as default_settings %}

  {% set elasticsearch = salt['pillar.get'](
    'elasticsearch',
    default=default_settings.elasticsearch,
    merge=True
  ) %}

Random notes
------------

* "module.wait" can be used to wait for an event on the event bus, and act accordingly. For example you can watch for a job changing a file on the event bus, and call the service.restart state module.
