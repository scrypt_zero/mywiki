kubernetes-FR
=============

Introduction
------------

* Kubernetes est un outil d'automatisation de conteneurs.
* Il permet de gérer facilement une infrastructure basée sur des conteneurs, en réduisant des opérations manuelles fastidieuses.
* Maintenant Kubernetes sait aussi gérer des VM, et les gère sur le même modèle que les conteneurs.
* Haute Dispo: Kubernetes s'assure qu'il y a toujours un nombre suffisant de conteneurs UP pour rendre le service, et qu'ils sont correctement répartis sur plusieurs serveurs, afin d'éviter de perdre toute une stack sur un soucis matériel.
* Elasticité: on peut facilement faire du scale up ou down, suivant la charge de l'infrastructure. Kubernetes peut se charger de spawner ou tuer des conteneurs suivant des métriques de charge par exemple.
* Déploiement continu: des conteneurs contenant la nouvelle version d'un applicatif peuvent être déployés au fur et à mesure, en remplaçant petit à petit les anciens conteneurs (rolling release).
* Kubernetes est écrit en GO et le fruit de plusieurs années d'expérience de Google à travailler avec des milliards de conteneurs à gérer.
* Il faut nécessaire un moteur de conteneur sur le Kubernetes maitre et les nodes. En général, c'est le docker Engine, mais on peut aussi mettre du rkt ou containerd.
* Attention aux versions du docker engine utilisés, qui doivent être supportés notamment par le kubeadm lors de la création du cluster.

Installation d'un cluster
-------------------------

* Le plus simple est d'utiliser kubeadm qui est un outil qui automatise la grande majorité de l'installation.
* Sur le ou les masters, et les nodes, il faudra installer: kubectl, kubeadm, kubelet, et évidemment docker engine ou équivalent.
* Kubelet est fait office d'interface entre kubernetes et le docker engine. Il gère les conteneurs sur chaque noeuds, leur cycle de vie.
* Une fois les 4 composants installés sur chaque membre du cluster (master et nodes), il faut faire un kubeadm init sur le maitre, puis configurer kubectl. On pourra verifier avec kubectl que le serveur kubernetes répond bien (il faut voir CLIENT VERSION et SERVER VERSION). Puis il faudra faire un kubeadm join sur chaque des nodes.
* Une fois cela terminé, il n'y a plus qu'à faire un kubectl get nodes sur le maitre pour vérifier qu'il voit bien les nodes. A ce stade, les nodes sont en NOT READY ce qui est normal car on a pas configuré le réseau.

.. code-block:: bash

    sudo kubeadm init --pod-network-cidr=10.244.0.0/16

.. code-block:: bash

    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config

.. code-block:: bash

    kubectl version

.. code-block:: bash

    sudo kubeadm join $some_ip:6443 --token $some_token --discovery-token-ca-cert-hash $some_hash

* Si on a perdu la commande de join, on peut la régénérer

.. code-block:: bash

    kubeadm token create --print-join-command

.. code-block:: bash

    kubectl get nodes

* On va ensuite installer la couche réseau. On va se baser sur le plugin flannel (développé par coreos). Il s'agit de charger cet objet sur le master, qui va automatiquement spawner 3 conteneurs flannel, et passer le master et les nodes en ready. 
* Il faut au préalable passer une option au kernel des noeuds (à détailler le pourquoi)
* On va enfin vérifier que les noeuds sont passés en ready, et que les conteneurs flannels sont démarrés, en regardant les pods dans le namespace kube-system.

.. code-block:: bash

    echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
    sudo sysctl -p

.. code-block:: bash

    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
    kubectl get nodes

.. code-block:: bash

    kubectl get pods -n kube-system

Architecture
------------

* Le controller (ou les controllers ...) ont la particularité d'avoir un Control Plane. Ce control plane contient plusieurs composants essentiels au cluster.
* ETCD: une base de donnée distribuée, qui permet de partager des informations entre tous les controllers du cluster (les informations sur tous les objets, pods, nodes etc.)
* KUBE-APISERVER: frontal API en HTTP REST. Tout passe par là, même quand on utilise kubectl.
* KUBE-CONTROLLER-MANAGER: contient plusieurs services backends essentiels
* KUBE-SCHEDULER: détermine quand démarrer des pods, quand les détruire, sur quels noeuds etc.

* D'autres service sont également placé sur tous les nodes:
* KUBELET: qui fait office d'intermédiaire entre le moteur de conteneur et l'API kube. Il s'agit d'un service systemd (ce n'est pas un pod en tant que tel)
* KUBE-PROXY: il y en a un pour chaque noeud. Il gère les communications réseau entre les noeuds, notamment en gérant des règles de firewall.
* La plupart de ces composants peuvent être vus avec kubectl get pods -n kube-system

Pods
----

* Dans Kubernetes, tout est objet
* Un pods est la plus petite unité de travail de kubernetes. Il s'agit d'une unité contenant: un ou plusieurs conteneurs (si on a vraiment pas le choix que de les regrouper), du stockage, et une adresse IP.
* Chaque pod fonctionne dans un namespace. Si on en spécifie pas, le pod tombera dans le namespace par défaut.
* Pour décrire un pod, on va utiliser du YAML. Il n'y aura plus qu'à appeler kubectl create pour instancier ce pod.
* Kubectl describe est très utile pour avoir des informations sur un pod, mais aussi sur n'importe quel objet kubernetes, ainsi que kubectl get.
* Avec describe on va pouvoir voir les evenements associés au cycle de vie du pod, très utile pour du troubleshoot, ainsi que le node sur lequel il tourne.
* Le nom de l'image va déterminer quel image docker on va aller mettre dans le ou les conteneurs du pod décrit.

.. code-block:: yaml

  apiVersion: v1
  kind: Pod
  metada:
    name: nginx
  spec:
    containers:
    - name: nginx
      image: nginx

.. code-block:: bash

  kubectl create -f test-pod.yml
  kubectl describe pod nginx
  kubectl delete pod nginx

Nodes
-----

* Kubernetes fonctionne en cluster, avec deux types de nodes: un ou plusieurs controllers (qui expose l'API, et on des pods dédiés au contrôle de l'ensemble du cluster), et les workers, qui portent uniquement des pods d'applications diverses.
* On peut faire un kubectl get nodes ou un kubectl describe node <hostname>, qui va nous permettre de voir l'état de chaque node, le cpu et la ram consommée, les événements du cycle de vie du node etc.

.. code-block:: bash

  kubectl get nodes
  kubectl describe node server1

Réseau
------

* Kubernetes dispose d'un réseau virtuel pour l'ensemble de son cluster, qui se place au dessus du "vrai" réseau.
* Un pod peut contacter un autre pod, dans un même CIDR, sans se soucier du fait qu'il n'est pas sur le même node.
* On peut utiliser un pod "busybox", qui permet de lancer curl, afin de diagnostiquer des problèmes de comm entre pods.
* Il existe différent plugin réseau pour Kubernetes, Flannel étant un des plus connu.
* Pour voir les IP des pods, on peut ajouter l'option wide à kubectl get

.. code-block:: yaml

  apiVersion: v1
  kind: Pod
  metadata:
    name: busybox
  spec:
    containers:
    - name: busybox
      image: radial/busyboxplus:curl
      args:
      - sleep
      - "1000"

.. code-block:: bash

  kubectl get pods -o wide
  kubectl exec busybox -- curl 10.200.1.1
