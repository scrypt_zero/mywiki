systemd
=======

History
-------

* Bios => Boot sector (grub ...) => linux kernel => init ramdisk (removed after) => init system (init sysv, systemd ...)

* Reminder init: init was created for Unix SystemV and BSD, Linux distributions used SysV version the most.
* /sbin/init read /etc/inittab to determine the runlevel. Each runlevel has a group of services predefined.

.. code-block:: bash

    Init 0 => halt
    Init 1 => single user (root) for maintenance
    Init 2 => multi, no network
    init 3 => multi with network
    init 4 => non used
    init 5 => multi with network and gui
    init 6 => reboot

* /etc/inittab is composed like this:

.. code-block:: bash

    identifier : runlevel : action : process
    id:3:initdefault:  # start init 3 by default
    si: : sysinit : /etc/rc.d/rc.sysinit # start sysinit in all runlevels
    15:5:wait:/etc/rc.d/rc 5 # init enter level 5 and wait for the rc scripts termination

* rc is acronym for run command
* rc.sysinit scripts are runned before the others
* rc.local is for customs by the administrator
* /etc/rc* files are link to /etc/init.d/* files, with a command (start, stop) and a priority.

* chkconfig and service are tools to facilitate init management (chkconfig for determine service activation in specific runlevels, service to stop/start services)

* The main problem with sysv scripts are the start orders. Services are started sequentially, so the start process is slow, and order management is difficult.

* upstart was developed for ubuntu first, as an attempt to replace sysv init.
* upstart can monitor the services, and start multiple services simultaneously.
* upstart uses a /sbin/init too, in order to maintain compatibility.
* upstart is dynamic and can trigger jobs after an event occurs (add a screen for example)
* upstart is not really used anymore.



Purpose of systemd
------------------

* Inspired by upstart and apple launchd
* Released in 2015 on Fedora
* Get rid of the "a before b" dependency tree (upstart design issue)
* All service "sockets" are created at once, and so a service don't have to wait for another socket service to open.
* systemd is like a "power strip outlet" and a bus, queuing messages between processes, so you can replace a service "live" and not lose messages.
* systemd log system is designed to NOT lose any logs, even when there is a crash.
* systemd objectives: faster bootimes (parallel start of services), improve robustness (bus system with queuing), improved logging system for debugging purpose.

Architecture
------------

* All processes are now managed in cgroups (cpu/ram/io ...)
* /sys/fs/cgroup/systemd show the "slices", a group of services.
* We can found system.slice (crond, httpd, tmp ...) and user.slice (user specific services: desktop ...)
* systemd score are set of processes that are started by other processes (childs)
* systemd-cgls show the breakdown of processes, in a tree maner
* Examples: user.slice => user-1000.slice => session-2.scope
* every user that logs in get its own user.slice
* /sbin/init still exist, but it's a link to systemd executable (systemd get pid 1)
* traditional filesystems were bring up with /etc/fstab (one at a time, fsck on each one, all FS had to be loaded before a service could start)
* systemmd uses an approach based on autofs: only mounts a fs when it is needed, and autofs create "fake" FS to permit a service to get queued to start.

Controversies
-------------

* abandon init
* not simple as the linux philosophy ?
* binary log file (no traditional text files)
* "feature creep" (do more thing that the basic objective of an init system)
* not portable (bsd ... yet)
* Debian switch to systemd in 205, but a fork called Devuan exists (sysvinit, openrc, sinit ...)
* OpenRC start in 2007, and it is not a replacement to init, but it works with it. Utilize cgroups like systemd (gentoo, alpine by default)
* another alternative: runit
* systemd is now user on the vast majority of commercial linux

Useful commands
---------------

* systemd uses all components of the os as a "unit": something.unit (user.slice, dbus.socket ...)
.. code-block:: bash

    systemd-cgls # list processes, slices, scopes .. very similater to systemctl status
    systemd-cgtop # show cgroups cpu/memory ... usage per process
    systemctl status | enable | disable | start
    systemctl # only, show loaded and active units
    systemctl --all # (add not loaded units)
    systemctl help service # show all options for the target service
    systemctl -H host command # you can execute remote systemctl commands

* Enable create links from the reference service file in /usr/lib to /etc/systemd
* "Vender preset" means that the package maintainer chose NOT to enable this service by default
* You don't need to procie the httpd.service unit, just httpd (but you can't if you have another unit with the same name)

journald
--------

* systemd journal is a binary file, collecting kernel, system, service, audit logs ...
* this journal is stored in /run/log/journal by default, and purged at reboot
* it can be saved by creating /var/log/journal
* it can be configured in /etc/systemd/journald.conf
* store = auto (/run/log/journal AND /var/log/journal if it exists), persistent (/var/log/journal), volatile (memory), none (trash)
* We can configure disk and ram usage (per journald file or as a all): systemMaxuser, runtimeMaxuse, systemMaxfilesize, runtimeMaxfilesize etc.
* MaxretentionSec is another way of keeping disk usage in control
