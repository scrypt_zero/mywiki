kubernetes
==========

Documentations / Tools for learning
-----------------------------------

* https://kubedex.com/kubernetes-courses/ : list of resources
* https://www.magicsandbox.com/ : interactive learning (basics)
* API Reference: https://kubernetes.io/docs/reference/#api-reference
* Yaml basic doc: https://learnxinyminutes.com/docs/yaml/
* Kubernetes addons: https://kubernetes.io/docs/concepts/cluster-administration/addons/


General knowledge
-----------------

* Container / microservice platform, portable, cloud native
* Features common to PaaS: deployment, scaling, load balancing, logging, monitoring
* Source code is hosted by the CNCF (Cloud Native Computing Foundation)
* Automatically manage resource requirements and constraints
* Self-Heal: stop non responsive containers, start new container, slow start of services ...
* Horizontal scaling, with yaml declaration, CLI, automatically (cpu or memory usage ...)
* Load balance and auto discover (service abstraction with their own IP and DNS)
* Automatic rollout / rollback (don't kill previous application version containers at the same time ...)
* Manage external secrets (not in the container images !!), can update them live (no container restart)
* Storage are automatically mounted (public cloud store, local storage, network storage ...)
* If an application can be containerized, it can run on kubernetes (but not necesseraly cloud natively !!)
* Kubernetes need you to build CI/CD workflow nonetheless (kubernetes do not build or package applications)
* Kubernetes does not provide middlewares such as webservers, database, message bus ...
* Logging / monitoring / alerting are not dictate by Kubernetes, but it provides mechanisms to collect and export metrics
* All is object in Kubernetes, and Objects are describes in YAML
* A yaml file is a declarative description of a desired state, a "record of intent"
* The field "apiVersion" "kind" "metadata" and "spec" are mandatory
* API is the main tool to discuss with Kubernetes. The API is used by devops as well as internally
* 'Kind' determines the object type
* 'apiVersion' determines the object "schema" (see the API Reference)
* Objects need to have on or more labels
* We can describe several objects in the same file, separeted by "---". For example a pod and a service exposing it.

The Master
~~~~~~~~~~

* The control plane is what affect Kubernetes cluster and is managed by the Master. It can take global decisions: detect events on the cluster and act accordingly (spawn a replica ...)
* The master is usually running on a separate machine than the users containers
* Main master components: kube-apiserver (the control plan frontend), etcd (all cluster data, need to be backed up), kube-scheduler (watch new nodes and affect them to machines, based on a lot of parameters and constraints), kube-controller-manager (run the controllers)
* The controllers use a watching loop and try to match the desired state of the cluster (single process)
* Different controllers: nodes (if a node is down ...), replica (number of replica ...), endpoint ( populate service and pods), service account and token controllers, cloud-controller-manager (interact with the cloud providers)

The Node
~~~~~~~~

* Node components are running on every node (virtual or physical machine), maintain pods and provide kubernetes runtimes
* "kubelet" ensure that every pod are as describes in their states, and healthy
* kube-proxy is a network abstraction for performing connection forwarding
* Container runtime manages ... containers (docker, rkt runc ...)

Addons
~~~~~~

* Non-essential cluster features, like DNS, dashboard, logging framework ...

Namespaces
~~~~~~~~~~

* Namespaces are used to create virtual clusters backed by the same physical cluster. A namespace IS a virtual cluster.
* In different namespaces you can have ressources with the same names.
* Virtual clusters are used to divide the real cluster between multiple users.

.. code-block:: bash

    kubectl get namespaces

Pods examples
-------------

* A pod is a group of one or more docker containers. It's the base building block of kubernetes
* You can have multiple containers in a pod if there are tighly coupled (shared ressources, network on localhost ...) otherwith you should keep them separated (update are more easy that way ...)

Define a pod in yaml:

.. code-block:: yaml

    apiVersion: v1
    kind: Pod
    metadata:
      name: nginx
    spec:
      containers:
      - name: nginx
	image: nginx:1.7.9
	ports:
	- containerPort: 80


.. code-block:: bash

    kubectl get pods # list pods and their states
    kubectl describe pod nginx # describe events, ports, mounts ... for a given pod
    kubectl delete pod nginx

* Containers spec in a pod cannot be updated (you have to kill et restart a pod)
* You can use volumes to store secrets and configurations map, used in the container initialization.

.. code-block:: yaml

    apiVersion: v1
    kind: Pod
    metadata:
      name: nginxhttps
      labels:
	name: nginxhttps
	app: nginx
    spec:
      volumes:
      - name: secret-volume
	secret:
	  secretName: nginxsecret
      - name: configmap-volume
	configMap:
	  name: nginxconfigmap
      containers:
      - name: nginxhttps
	image: ymqytw/nginxhttps:1.5
	command: ["/home/auto-reload-nginx.sh"]
	ports:
	- containerPort: 443
	- containerPort: 80
	livenessProbe:
	  httpGet:
	    path: /index.html
	    port: 80
	  initialDelaySeconds: 30
	  timeoutSeconds: 1
	volumeMounts:
	- mountPath: /etc/nginx/ssl
	  name: secret-volume
	- mountPath: /etc/nginx/conf.d
	  name: configmap-volume


Deployment examples
-------------------

* A deployment manages multiple pods. The "deployment controller" make sure the reality match the state described.
* A controller called "Replication Controller" ensures that the specified number of pod replicas are running.
* In fact a deployment IS a ReplicaSet object

Define a state in yaml:

.. code-block:: yaml

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: nginx-deployment
      labels:
	app: nginx
    spec:
      # number of desired pods
      replicas: 3
      selector:
        # label selector for pods (template label must match)
	matchLabels:
	  app: nginx
      # describes the pods
      template:
	metadata:
	  labels:
	    app: nginx
	spec:
          # list of containers inside a pod
	  containers:
	  - name: nginx
	    image: nginx:1.7.9
	    ports:
	    - containerPort: 80

Apply the state:

.. code-block:: bash

    kubectl apply -f deployment.yml

.. code-block:: bash

    kubectl get deployment # list deployments and their states

* Parameters like replicas number or image version can be updated and applied live (more replicas spawned, old images replaced by new images without service downtime ...)

Services examples
-----------------

* A service is an abstraction of a logical set of Pods, it can be view as a micro-service.
* Frontends pods can called backend pods serving a mysql database for example. They need to know the ip of the Service but not the ip of each pods, because pods are "mortals" and can be shutdown by a replicaSet controller. But the service will keep running.
* A service is an object like the others. It will select the pods by their matching labels.


Define a service in yaml:

.. code-block:: yaml

    apiVersion: v1
    kind: Service
    metadata:
      name: my-service
    spec:
      # select pods with labels
      selector:
	app: MyApp
      ports:
      - protocol: TCP
        # service listening port
	port: 80
        # destination port on pods
	targetPort: 9376

.. code-block:: bash

    kubectl apply -f service.yml
    kubectl get services


Useful commands
---------------

.. code-block:: bash
 
    kubectl cluster-info # show the state of the kubernetes nodes and services
    kubectl api-versions # show supported api versions on this cluster
    kubectl logs pods test # show logs for a pod named test
    kubectl get pod/logger service/nginx-service # for multiple objects, we have to use "/" after the types
    kubectl scale deployment test --replicas=2 # change the number of replicas for deployment "test" to 2
    kubectl get pods -l app=web # get pods with labels app:web
    kubectl get pods -o json # -o specify output type, here json format
    export POD_NAME=$(kubectl get pods -o=jsonpath="{.items[0].metadata.name}") && kubectl label pod $POD # rename a pod label
    kubectl label pod $POD_NAME app=nginx

* API usage with curl

.. code-block:: bash

    curl -X POST -H "Content-Type: application/yaml" --data-binary "@pod.yaml" localhost:9992/api/v1/namespaces/default/pods
