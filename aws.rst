aws
===

Core concepts
-------------

* An amazon region: a group of availability zone. An availability zone = one (or more) DC in a specific locality. Availability zone communicates between them (share buckets content and so on). You should use availability zone closer to you (best latency).
* VPC (Virtual Private Cloud) is our own network inside global aws architecture. A portion of isolated network, with your own ressources. You have a default VPC at creation of your account.
* Cloudwatch is the main monitoring tool for all your resources and billing
* Resource Groups is a collection of ressources (instances, storage ...) based on tags. For example we can create a "production" tag and add ressources to it.
* IAM (Identify and Access Management): manage AWS users and their access to resources. When you create your account, you are ROOT, with all access. You can create other users, and by default they have no rights, you must grant them.
* Each resources in AWS have an ARN: Amazon Resource Name, a unique identifier (users, instances, groups ...)

Billing
-------

* Even in free tier you need a valid credit card
* Some free tiers services expires after 12 months, others are unlimited in time. You need to recreate another account.
* You should enable billing alarms in your accout settings (mail notification when you use more than free tier boundaries, or with cloudwatch alerts on a specified threshold)
* When creating a billing alarm with cloudwatch (on the metric "EstimatedCharges" for example), you need to be in us-east region
* You should use a Notification List instead of notifyMe, create or use an existing Notification List, and add your email to it (sort of mailing list, with subscribers).

Navigation
----------

* You can use the "pinpoint" in the top bar to add shortcuts for services.
* In "Alert" "View all alerts", you can see all incidents in various AWS regions, and in the details see if any of your resources were concerned.
* In Support, you can open cases and see your current support plan (basic, enterprise ...), which determines aws support reactivity and coverage ...

IAM
---

* You should use MFA on your root account
* You shoud rotate your administrative access keys
* You should delete your defaults administrative keys and create a new IAM user with administrative permissions
* You should put your IAM users in groups and apply the "least privilege"
* You can use CloudTrail to watch your IAM accounts activities
* Create a strong password policy for your IAM users
* For application credentials you should use roles (IAM accounts that are not users)
* IAM best practices in video: https://www.youtube.com/watch?time_continue=24&v=_wiGpBQGCjU
* You can read best practices here: https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html
* You can attach policies directly to a user, but it is best to use groups for policies reviews and organization.
* IAM users do not use the same URL to access the console as the root user.
* Roles are use for example to give your EC2 instances full acess to you S3 buckets, but they have other features.
* All IAM accounts are global and not linked to a particular AZ or Region.
* You can use Access Advisor in a user account details to see his resources access logs.
* A policy is just another json files listing rights on resources.
* Inline policies are linked to a user/group/resources, and not shared, each items has his own copy of the policy. So if you make a change to an inline policy, and you want them applied to all items with this policy "copy", you need to edit ALL clones of theses policy.
* Managed policies are shared and reusable, so best used over inline policies. They are maintained by Amazon or by administrators (when created by them). When you edit a managed policy, it applies on all items with this policy configured.
* Inline policies are useful if you want to maintain a strict one-to-one relationship between a policy and the principal entity that it's applied to. 
* For more details: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html

VPC & Network
-------------

* Basic VPC architecture: A subnet (public) with EC2 instances in it => NACL (Network Access Control List which is a virtual firewall) => route table (determine if you the trafic has to be sent to the internet or another VPC ...) => Internet Gateway
* A VPC can be shared between AZ. You should put your EC2 instances in different AZ for maximum tolerance.
* IGW (Internet Gateway) are like a modem with ISP connectivty. It is redundant and highly available. A default VPC always have an IGW.
* A VPC can only have one IGW and you can't detached it if they are active resources in your VPC.
* A default main route table is created with your VPC.
* You can have multiple route tables in your VPCs. If you have subnet depending on a route table, you cannot delete the route table.
* When you create a VPC, a subnet is created for each AZ you have in your region (EU-WEST for example has 3 subnets by default).
* Inbound rules in NACL controls IN stream for your subnet or subnets, and outbound ...
* Rules are processed in order like in any firewall table. You need to have an outbound matching your inbound for response.
* When you create a new NACL list you have to associate it to one or many subnets.
* NACL are stateless (you need to explicitely authorize the response).
* A subnet can only be link to ONE NACL. But an NACL can be shared between subnets.
* Additional Security Groups can be activated on your resource, in addition of NACL rules.
* A subnet must reside entirely in ONE AZ, you can't span it. But you can have multiple subnets in one AZ.
* You can have private and public subnets. Public subnets have a route to the internet, private subnets don't. Typically an front web EC2 instances will be in a public subnet, and an RDS database for backend, in a private one. In private, all communications are kept inside a VPC.
* A subnet is implicitely associated with the main route table.
* You should use as much as possible AZ redundancy for your resources. Some resources are automatically replicated between the AZ of a same region (like S3), some are not and you need to set them yourself.

EC2
---

* AMIs are Amazon Machine Images (Windows or Linux-like ...)
* Instance Type determines processing power.
* EBS is local storage (think volatile hdd).
* IP Addressing is the equivalent of a network card.
* Firewall is Security Groups.
* EC2 Types: On-Demand (most expensive, but most flexible). Provision/terminate it at any time (on-demand). Reserved: you own it for a set time period (one or three years ...). Significant price discount. Spot: you "bid" on an instance type. You only pay and use that instance when the spot price is equal or below your bid price.
* More details: https://aws.amazon.com/fr/ec2/pricing/
* You can also have dedicated hosts.
* Also https://calculator.s3.amazonaws.com/index.html
