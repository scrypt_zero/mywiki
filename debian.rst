Debian
------

.. code-block:: bash

  apt-get update && apt-get dist-upgrade && apt-get autoremove && apt-get autoclean && apt-get clean
