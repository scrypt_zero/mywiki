ansible
=======

.. code-block:: bash

  ansible all -m "ping" -i INV -u root
  ansible all -m "setup" -i INV -u root | grep ansible_distributions

.. code-block:: bash
  ansible-galaxy install --ignore-certs --role-file requirements.yml --force --roles-path roles
