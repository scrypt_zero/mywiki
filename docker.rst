docker
======

Installation
------------

.. code-block:: bash

    docker --version

    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo

    yum repolist
    yum update
    yum install docker-ce
    yum list installed | grep docker

* connection to docker socker: need to be in group docker

.. code-block:: bash

    usermod -aG docker user

Images & Containers management
------------------------------

.. code-block:: bash

    docker --version
    docker search centos # search on docker hub or other repositories
    docker pull hello-world.
    docker run docker/whalesay cowsay hello
    docker images
    docker container ls -a # or docker container ps -a
    docker ps -n 2 # show last 2 containers
    docker pull centos:latest # use the latest version
    docker pull centos:centos6 # use centos6 tag
    docker inspect centos # decribes image in json format
    docker stop|kill|rm id
    docker rmi myimmage # use -f to force if containers are referencing that image
    docker ps -a -q # show all containers (stopped and running), ids only (quiet mode)
    docker rm $(docker ps -a -q) # delete all containers
    docker rmi $(docker images -q) # delete all images
    docker stop $(docker ps -q) # stop all running containers
    docker restart myid

* We only delete the base image with rmi, but the container referencing it can still start without it (snapshots are created and contains the base image nonetheless). You just lose the reference to that base image.

* You need to delete the containers as well to gain space.

* IP addresses are automatically assigned from the range of docker0 interface

Specifics runs
--------------

.. code-block:: bash

    docker run -it centos:latest /bin/bash # i for interactive, t for tty
    docker run -d centos:latest /bin/bash # detached mode (background)
    docker run -d --name=myweb1 nginx # name must be unique
    docker run -d nginx:latest
    docker inspect monid | grep IPAdd
    curl 172.17.17.0.2:80
    docker attach myid # if no bash is previously spawned, the container will terminate on ctrl+c, you should exec a bash first
    docker exec -it myid /bin/bash

Volumes & Network
-----------------

.. code-block:: bash

    docker run -d -p 8081:80 --name=webserver1 -v /var/www:/user/share/nginx/htmlm nginx:latest # map local /var/www to nginx directory, and use port translation of local 8081 to 80 in the container.
    docker port myid # show port translations for a specific container
    docker run -d -P nginx:latest # map ALL exposed container ports to the underlying host (in the range 32768 to 65000)

.. code-block:: bash

    docker network ls # list interface viewed from docker
    docker inspect bridge # inspect a specific network (subnets and so on)

.. code-block:: bash

    docker run -it --name voltest4 -v ~/builds/myhostdir:/metada centos:latest /bin/bash
    docker run -it --name voltest5 -v /mymount:/targetmount centos:latest /bin/bash

* When you use -v without a source volume, it use a random directory in /var/lib/docker/volumes.
* You can't use a specific fs to fs mount if you use Dockerfile, as it needs to remain portable.

Docker files examples
---------------------

* We always start a dockerfile based on something, except in rare occasions where we do it from scratch (OS images for example)
* By default Docker use the cache when it builds an image to save space

.. code-block:: yaml

    FROM debian:stable 
    MAINTAINER scrypt <scrypt@scrypt.com>

    RUN apt-get update && apt-get upgrade -y && apt-get install -y apache2 telnet curl  

    EXPOSE 80

    ENV MYVALUE my-value    

    CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]  

* Each instruction in the dockerfile create a layer, which can be cached
* Combining multiple installation in a RUN is a best practice (apt-get update && apt-get upgrade ...) to gain disk space

.. code-block:: bash

  docker build -t scrypt/myapache
  docker run -d scrypt/myapache
  docker exec -it myid /bin/bash

* The difference between RUN and CMD, is that CMD are launched after the container is initialized, and RUN in the initialization process.

* For example, a RUN directive will create a layer, but the CMD don't. The CMD is executed after the container start. Typically we will use the CMD command to launch daemons.

.. code-block:: bash

  docker history myimage # show history of an image build
  docker commit mycontainer myimage # save the changes in the container to a new image (Dockerfiles are a best practice)

* A docker file MUST start with FROM directive in the first place

* Root user always exist but we can force the user entry to another user, like this:

.. code-block:: yaml

  FROM centos:latest
  MAINTAINER scrypt <scrypt0@gmail.com>

  RUN useradd -ms /bin/bash myuser
  USER myuser

* To exec a bash as root when the default user is unpriviled

.. code-block:: bash

  docker exec -it -u 0 mycont /bin/bash

* The order of RUN in the dockerfile matters. For example if you try to write in /etc AFTER creating a non privileged user, your build will fail.

* The ENV directive set a variable system wide, not to a particular USER

.. code-block:: yaml

  FROM centos:latest
  MAINTAINER scrypt0@gmail.com

  RUN yum update -y
  RUN yum install -y net-tools wget

  RUN cd ~ && wget https://rpmfind.net/linux/centos/7.6.1810/os/x86_64/Packages/java-1.8.0-openjdk-1.8.0.181-7.b13.el7.x86_64.rpm

  RUN yum localinstall -y ~/java-1.8.0-openjdk-1.8.0.181-7.b13.el7.x86_64.rpm

  RUN useradd -ms /bin/bash user
  USER user

  ENV JAVA_BIN /usr/java/jdk1.8.0/jre/bin

* ENTRYPOINT: force a command each time a container is run. You can still execute other commands but this one will be executed nonetheless. Very use for services. Entrypoints should be used every time you need a container to be run as an executable.

.. code-block:: yaml

  FROM centos:latest
  MAINTAINER scrypt0@gmail.com

  RUN useradd -ms /bin/bash user

  ENTRYPOINT ["/bin/echo"]
  CMD ["default"]

  USER user

* If you specify parameters at the start of the container, the parameters will be passed at the end of the entrypoint

.. code-block:: bash

  docker run centos7/myentry:v1
  >> default # the CMD is passed as argument to the entry point
  docker run centos7/myentry:v1 test
  >> test # test is passed as a parameter to the entry point, overriding the CMD

* Difference between shell form and exec form:
* The docker shell syntax (which is just a string as the RUN, ENTRYPOINT, and CMD) will run that string as the parameter to /bin/sh -c. This gives you a shell to expand variables, sub commands, piping output, chaining commands together, and other shell conveniences. Example:

.. code-block:: bash

  RUN ls * | grep $trigger_filename || echo file missing && exit 1

* The exec syntax simply runs the binary you provide with the args you include, but without any features of the shell parsing. In docker, you indicate this with a json formatted array. Example:

.. code-block:: bash

  RUN ["/bin/app", "arg1", "arg2"]

* httpd service example:

.. code-block:: yaml

  FROM centos:latest

  MAINTAINER scrypt0@gmail.com

  RUN yum update -y && yum install -y httpd net-tools

  RUN echo "This is a custom index file build during image creation" > /var/www/html/index.html

  EXPOSE 80

  ENTRYPOINT apachectl "-DFOREGROUND"

.. code-block:: bash

  docker run -d --name apache1 -P centos7/apache:v1 # -P will NAT every exposed ports to local safe ports range
