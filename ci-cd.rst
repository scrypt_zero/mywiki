CI/CD
=====

Appli node.js utilisée
----------------------

* Installation du moteur nodeJS et npm
 
.. code-block:: bash

  curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
  chmod +x nodesource_setup.sh && ./nodesource_setup.sh
  sudo apt-get install -y nodejs
 
* https://github.com/ferrynathan/cicd-pipeline-train-schedule-git

.. code-block:: bash

  npm install
  npm start

* Appli exposée sur http://<ip>:3000
* Node.js permet de faire du javascript côté serveur. Langage bas niveau, plus proche du C que du php. Censé être rapide grâce à un moteur d'exécution hérité de google chrome, et asynchrone 'non bloquant', orienté événement, monothread, le rendant intéressant pour de l'appli temps réel type chat.
* npm est le package manager de node.js. Il permet notamment d'aller automatiquement chercher des modules et leurs dépendances (équivalent cpan python), pour les installer dans nodes_modules. Il génère ensuite le package-lock.json qui contient l'historique de ce qui a été pull.
* npm install va aller télécharger les modules et dépendances déclarées dans le package.json
* npm start va lancer le script déclaré sous start dans le package.json (ici bin/www)
* Les routes permettent de faire matcher les fonctions qu'on a pu écrire avec les requêtes qui arrivent sur le serveur.

SCM (git)
---------

* Git est retenu ici (sur la plateforme github)
* Pas de détail sur l'usage de git car connu, mais le SCM est le socle d'un CI/CD, il est inenvisageable de construire un pipeline sans gestionnaire de code collaboratif à la racine.
* les fonctions de branches, tags, merge request vont faire partie de la base de travail et doivent être connues de tous

Build Automation (Gradle)
-------------------------

* Le rôle du build automation est de transformer le code source en un artefact livrable en production. Il comprend plusieurs étapes.
* Compiler les sources
* Evaluer et embarquer les dépendances nécessaires
* Passer des tests automatiques
* Packager le tout
* Un outil de build automatisé devient vite obligatoire sur des projets avec plusieurs développeurs. Il permet de tracer les builds, les tester, itérer dessus sans tout casser, s'assurer de leur intégrité, être sûr qu'on envoit en prod les bons fichiers, gérer plus facilement les dépendances (compileurs, librairies etc.), générer la documentation etc.
* Pour résumer un outil de build automatisé remplace les longues, fastidieuses et risquées ... manipulations qui font passer du code brut à un artefact déployable ...
* Gradle est un outil de build automatisé parmi tant d'autres (Maven, Ant ...). Ecrit en java, et utilise un langage DSL appelé Groovy. On peut aussi utiliser le langage Kotlin.
* Il prend en entrée des sources, des dépendances, et crée en sortie des artefacts. Il permet aussi de faire du build continue en restant à l'écoute en entrée de nouvelles sources par exemple, et déclenche alors un nouveau build.
* Article intéressant https://linchpiner.github.io/gradle-for-devops-1.html
* Notamment utilisé par Google pour ses projets Android. Gagne en popularité et vient titiller maven resté star sur les projets java
* Il demande un java jdk > 7 pour fonctionner 
* Il apporte pas mal de fonctionnalités permettant de rendre plus rapide les builds, surtout quand ils sont lancés X fois par jour par les devs (mécaniques de cache, d'idempotence ...)
* Exemple d'installation sous Debian 9: https://linuxize.com/post/how-to-install-gradle-on-debian-9/

.. code-block:: bash

  gradle -v

* Il est intéressant d'utiliser le Gradle Wrapper. Il s'agit d'un script qui va se déposer dans le dépot de code, à qui on va spécifier une version de Gradle donnée.
* Cela va permettre à n'importe qui (ou n'importe quel process) ayant cloné le dépôt, d'aller automatiquement télécharger la version de Gradle spécifiée, avec juste Java comme prérequis. On peut ainsi avoir des versions de Gradle différentes entre plusieurs projets et s'assurer qu'un projet sera construit avec telle ou telle version. On se décharge aussi de la gestion du téléchargement de Gradle comme prérequis.
* Dans le répertoire de mon projet:

.. code-block:: bash

  cd ~/my-project
  gradle init
  ./gradlew build
  echo ".gradle" >> .gitignore
 
* gradlew sera le binaire qu'on va appeller pour jouer les commandes gradle, au premier lancement il va télécharger gradle dans la version spécifiée dans le gradle-wrapper.properties. Comme on a rien dans le projet, on a un BUILD SUCCESSFUL
* gradle init va installer le wrapper dans le repo du projet, ainsi que tous les fichiers gradles nécessaires.
* La base de gradle est de jouer des tasks. Ces tasks peuvent être "customs" (écrites dans le fichier build.gradle), des tasks "built-in" de gradle, ou encore des tasks héritées de plugins.
* toutes ces tasks peuvent être mises en relations de dépendances, par exemple taskA.dependsOn taskB

.. code-block:: bash

  cat build.gradle
  tasks.register("hello") {
    group = 'Welcome'
    description = 'Produces a greeting'

    doLast {
        println 'Hello, World'
    }
  }
  gradelew hello

* Liste des tasks disponibles
 
.. code-block:: bash

  gradelew tasks

.. code-block:: bash

  task1.dependsOn task2

* Un build gradle n'est autre que de jouer un ensemble de tasks, qui peuvent avoir des dépendances entre elles. On exécute le script groovy build.gradle.

.. code-block:: bash

  task provision {
    outputs.dir("my_dir")
    doLast {
        // provisioning code
        file("my_dir").mkdirs()
    }
  }

* Dans cet exemple, le répertoire my_dir ne sera crée qu'une seule fois.

* Si on veut utiliser des plugins, il suffit de les déclarer dans le build.gradle. Il existe des plugins pour SSH ou pour AWS par exemple.

.. code-block:: bash

  cat build.gradle
  plugins {
    id "<pluginid>" version "<pluginversion>"
  }

* On pourrait faire des tasks Gradle qui appellent une api de configuration management (par exemple, salt ?) pour vérifier par exemple l'état d'une cible (tel paquet est-il installé ?) et éventuellement ne rien faire grâce à l'idempotence de l'outil de CM.

* Autre exemple, utiliser des variables d'environnement, qu'on peut part exemple mettre au format yaml, dans un fichier de configuration externe au fichier de build

.. code-block:: bash

  cat conf.gradle
  ext {
    host = "127.0.0.1"
    user = "root"
    port = 8080
  }
  cat build.gradle
  apply from: "conf.gradle"

  task checkConnection {
      description "Checks connection to ${host}:${port}"
      doLast {
          println "Checking connection to ${user}@${host}:${port} ..."
    }
  }

* Gradle (ou tout autre outil de build automatisé) permet de réaliser des tests automatiques à plusieurs niveaux.
* Des tests unitaires, sur des petits bouts de code (par exemple, une fonction)
* Des tests d'intégration, qui fait s'imbriquer différentes fonctions du code et vérifie leur imbrication
* des smoke test / sanity test, de plus haut niveau, qui vérifie le fonctionnement global de l'application (l'appli est up, tel endpoint renvoint un 200 etc.)
