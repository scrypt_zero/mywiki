CloudFormation
==============

Introduction
------------

* Permet de faire de l'infra as code
* Un template cloudformation peut être en json ou en yaml. On peut utiliser l'outil de design sur la console amazon pour passer de l'un à l'autre.
* On peut faire un design visuel de l'infra, qui sera convertie en json/yaml
* Un template cloudformation déploie des ressources, basiquement. Il gère l'idempotence en définissant un état, plutot que des actions.
* Un template cloudformation peut donc spawner une infrastructure complète.
* Cloudformation crée des stacks logiques, qui sont transformées en ressources "physiques"
* On peut par exemple créer une stack pour l'infra de production, avec un paramétrage pour la prod (cidr, user différents etc.)
* Puis une autre stack pour la préprod, qui prendra en entrée d'autres valeurs de paramètres
* Une stack est donc une instanciation d'un template cloudformation, avec éventuellement des paramètres particuliers.

Template de base
----------------

.. code-block:: yaml

  ---
  AWSTemplateFormatVersion: "2010-09-09" // permet de protéger le template de futures modifications par aws

  Description:
    this template does XXXX // optionnel, doit respecter le formatversion

  Metadata:
    template metadata // optionnel, permet notamment de partager des informations entre ressources

  Parameters:
    set of parameters // optionnel, infos que le template va demander (par exemple le nom de certaines ressources.) Peut contenir des defaults

  Mappings:
    set of mappings // optionnel, permet de mapper des variables suivant des environnements par exemple.

  Conditions:
    set of conditions // optionnel, cree des conditions pour la création de ressource par exemple

  Transform:
    set of transforms // optionnel

  Resources:
    set of resources // seul bloc obligatoire

  Outputs:
  set of outputs // définit ce qu'on va retourner au user

* Exemple de création de resource en json

.. code-block:: json

  {

  "Resources" : {

    "catpics" : {
      "Type" : "AWS::S3::Bucket"
      }
    }
  }

* Il n'est pas nécessaire de spécifier le nom physique d'une ressource, au contraire on va juste spécifier un logical ID (ici catpics)
* Quand la stack va être instanciée, elle va créer des ressources avec comme id unique <nomstack>-<logicalid>-<randomid>
* Ainsi le meme template peut être relancé un nombre infini de fois, sans jamais réecrire le même id de ressource (pratique pour du S3 ou un id de bucket doit être unique à travers tout amazon)
* On peut charger le template de différence façon: depuis son poste, depuis un bucket S3 ... on peut aussi utiliser le Designer ou des templates déjà faits par amazon.
* En plus des paramètres, on peut passer à la stack des policies, par exemple pour protéger telle ou telle ressource de futures modifications

* Pour updater une stack, on peut charger un nouveau template. Attention, si on recharge le template d'origine ensuite, on risque de supprimer des ressources: bien vérifier les changements qui vont êtr appliqués lors du résumé des opérations.
* On peut suivre la dérive d'une infra par rapport à sa stack cloudformation (par exemple si quelqu'un a modifié manuellement les paramètres d'un bucket)

.. code-block:: json

  {
  "Resources" : {
    "catpics" : {
      "Type" : "AWS::S3::Bucket",
      "Properties" : {
        "BucketName" : "catsareawesomeXXX"
      }
    },
    "dogpics" : {
      "Type" : "AWS::S3::Bucket"
      }
    }
  }

* Dans cet exemple, le fait d'updater la stack avec cette fois une propriete "bucketname" va positionner Replacement à True dans le changements. Cela signifie que la ressource physique existante pour le bucket de logical id catpics va être supprimée, puis recrée avec le bucketname catsareawesomXXX ... C'est donc très risqué, il faut faire attention aux opérations qui doivent supprimer et recréer une ressource, car ce n'est pas forcément dans les mêmes cas qu'avec la CLI.
* On peut utiliser https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html comme référence.

* ATTENTION: la suppression d'une stack CF va entrainer la suppression de toutes les ressources réelles associées.
