yum
===

Useful Commands
---------------

.. code-block:: bash

  yum repolist
  yum-config-manager --add http://monurl/file.repo
  yum info mypackage
  yum list installed mypackage
  yum localinstall mypackage.rpm
