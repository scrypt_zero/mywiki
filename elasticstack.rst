Elasticstack
============


* Historiquement, on parlait d'ELK: Elasticsearch, Logstash, Kibana
* Depuis, les outils beats (filebeat etc.) ainsi que les produits payants x-pack ont fait leur apparition.
* La stack ELK s'est donc enrichie, et est plutôt nommée elastic stack désormais

Beats
-----

* C'est la première étape de la collecte de log, il en existe plusieurs
* Les beats sont vu comme des "data shippers"

Filebeat
~~~~~~~~

* Filebeat: prend des fichiers de logs en entrée, chaque ligne de log devient un évenement
* Filebeat: sait gérer des listes d'exclusion (par exemple debug)
* Filebeat: sait gérer des logs multilignes (important sur des stacktrack java)
* Filebeat: sait gérer une rupture de la stack en aval, en gardant un pointeur sur la dernière log qui a été exportée
* Filebeat: sait gérer les logs de multiples containers
* Dans filebeat, on va déclarer des prospectors. Ce sont les agents qui vont aller scraper les logs à différents endroits. Par exemple, on peut avoir un prospector qui prend /var/log/\*.log. On peut déjà faire un premier filtrage à ce niveau, par exemple retirer les lignes en DEBUG. C'est aussi là qu'on va gérer le multiline.
* Au niveau des prospectors, on peut déjà enrichier les logs, par exemple en ajoutant des champs (de manière plus simple que logstash)
* Par exemple, je peux ajouter un champ "application1" sur tel log.
* Filebeat propose des modules d'ingestion tout fait, par exemple pour apache. On pourrait potentiellement se passer de logstash, puisque le découpage des logs aurait déjà été fait. Cela dépend du niveau de transformation des logs qu'on veut faire. Logstash assure aussi une pré-centralisation. Si on utilise pas logstash, il faudra utiliser des ingest nodes coté elastic, en frontal de filebeat.

Metricbeat
~~~~~~~~~~

* Metribeat: pourrait faire concurrence à prometheus, car il sait envoyer des metrics
* Metricbeat: cela peut etre des metrics de l'os, comme de stack comme docker, kubernetes, mongodb, nginx ...
* Metricbeat: il n'est pas dit que ce soit préférable à une stack prometheus / grafana, notamment parce qu'une base time series comme prometheus sera toujours plus rapide. De plus il y a tout les community dashboard sur grafana etc.

Packetbeat
~~~~~~~~~~

* Packetbeat: sait décoder et remonter des évenements réseau (icm, dns, http)
* Packetbeat: peut être utilisé pour mettre en correlation des évenements réseau, surtout des requetes/réponses

Heartbeat
~~~~~~~~~

* Heartbeat: est utilisé pour envoyer des requetes de type ping, tcp, http ... vers des endpoints, et verifier qu'ils sont up
* Heartbeat: sait gérer le TLS, l'authent, les proxy ... et faire des résolutions DNS

Auditbeat
~~~~~~~~~

* Auditbeat: très intéressant dans le cas des certifs pci/hds
* Auditbeat: sait remonter des évenements auditd
* Auditbeat: peut etre utilisé pour vérifier les hash de certains fichiers, remonter les changements, ou encore comparer les hash par rapport à des bases de hash malicieux (exemple: un binaire remplacé par un ransomware ...)

* Winlogbeat: remonte les évenements de l'eventlog

Logstash
--------

* Logstash est un "data pipeline", qui vient après les shippers.
* Il est basé sur une collection de plugins, pour chacun de ses trois briques.
* Logstash a trois briques: les inputs, les filters, et les outputs
* Les inputs: logstash va ingérer des evenements de multiples sources simultanément. Ca peut être du beat, du MQ, du log4j, de l'irc, du cloudwatch ... Par exemple si on a mis un tampon type rabbitMQ entre nos beats et notre logstash.
* Les filters: les évenements vont etre parsés (convertis en champs structurés), puis éventuellement enrichis. Par exemple on peut ajouter un champ avec l'ip de l'envoyeur, ou encore utiliser de la geoip. Un des filters les plus connu est le GROK qui permet de découper un champ en plusieurs autres, à partir de patterns (exemple: champs message haproxy). On peut aussi gérer les timestamps des logs à ce niveau
* Les outputs: on va stocker les évenements (stash), puis les envoyer à de multiples destinations (notamment elasticsearch, mais aussi du csv, du cloudwatch, encore du MQ, du nagios, du loggly ...)
* Pour résumer, Logstash va avoir pour role: de recevoir les logs (evenements) de multiples sources, de les structurer voire les enrichir (les évenements sont "préparés" pour la suite), puis enfin de les stocker en attente de leur envoi.
* Logstash est aussi basé sur la JVM, avec des possibilités de la tuner comme pour Elasticsearch.
* On va surtout créer des fichiers de pipelines. Un pipeline est un tryptique intput/filter/output, qui peut utiliser différent module et plugins. Par exemple on peut créer un input beat sur tel port, qui va ensuite utiliser le module apache et le plugin mutate pour retoucher les logs, et ensuite un input vers elasticsearch, avec un index daily.
* Il n'est pas forcément nécessaire de faire plusieurs pipelines. Cela dépend si on veut appliquer des flows / parametres de performances différents.
* Un seul pipeline peut servir à output différent index, basés sur le jour mais aussi par exemple le vpc émetteur.
* Pour savoir où en est le scrapping des logs, on peut regarder la registry dans /var/lib/filebeat. Il va nous dire quels logs ont été scrappés, et ou en sont les pointeurs. Filebeat sait gérer les rotations de logs, en gardant aussi une référence à l'inode.

.. code-block:: json

  input {
    beats {
      port => 5044
      host => "0.0.0.0"
    }
  }
  filter {
    if [fileset][module] == "apache2" {
      if [fileset][name] == "access" {
        grok {
          match => { "message" => ["%{IPORHOST:[apache2][access][remote_ip]} - %{DATA:[apache2][access][user_name]} \[%{HTTPDATE:[apache2][access][time]}\] \"%{WORD:[apache2][access][method]} %{DATA:[apache2][access][url]} HTTP/%{NUMBER:[apache2][access][http_version]}\" %{NUMBER:[apache2][access][response_code]} %{NUMBER:[apache2][access][body_sent][bytes]}( \"%{DATA:[apache2][access][referrer]}\")?( \"%{DATA:[apache2][access][agent]}\")?",
            "%{IPORHOST:[apache2][access][remote_ip]} - %{DATA:[apache2][access][user_name]} \\[%{HTTPDATE:[apache2][access][time]}\\] \"-\" %{NUMBER:[apache2][access][response_code]} -" ] }
          remove_field => "message"
        }
        mutate {
          add_field => { "read_timestamp" => "%{@timestamp}" }
        }
        date {
          match => [ "[apache2][access][time]", "dd/MMM/YYYY:H:m:s Z" ]
          remove_field => "[apache2][access][time]"
        }
        useragent {
          source => "[apache2][access][agent]"
          target => "[apache2][access][user_agent]"
          remove_field => "[apache2][access][agent]"
        }
        geoip {
          source => "[apache2][access][remote_ip]"
          target => "[apache2][access][geoip]"
        }
      }
      else if [fileset][name] == "error" {
        grok {
          match => { "message" => ["\[%{APACHE_TIME:[apache2][error][timestamp]}\] \[%{LOGLEVEL:[apache2][error][level]}\]( \[client %{IPORHOST:[apache2][error][client]}\])? %{GREEDYDATA:[apache2][error][message]}",
            "\[%{APACHE_TIME:[apache2][error][timestamp]}\] \[%{DATA:[apache2][error][module]}:%{LOGLEVEL:[apache2][error][level]}\] \[pid %{NUMBER:[apache2][error][pid]}(:tid %{NUMBER:[apache2][error][tid]})?\]( \[client %{IPORHOST:[apache2][error][client]}\])? %{GREEDYDATA:[apache2][error][message1]}" ] }
          pattern_definitions => {
            "APACHE_TIME" => "%{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{YEAR}"
          }
          remove_field => "message"
        }
        mutate {
          rename => { "[apache2][error][message1]" => "[apache2][error][message]" }
        }
        date {
          match => [ "[apache2][error][timestamp]", "EEE MMM dd H:m:s YYYY", "EEE MMM dd H:m:s.SSSSSS YYYY" ]
          remove_field => "[apache2][error][timestamp]"
        }
      }
    }
  }
  output {
    elasticsearch {
      hosts => localhost
      manage_template => false
      index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
    }
  }

Elasticsearch
-------------

Introduction
~~~~~~~~~~~~

* Elasticsearch est le coeur de l'elastic stack. 
* Il est pensé en modèle distribué, notamment pour répartir des les tâches d'indexation, de recherche etc. sur x noeuds. On peut donc le scaler en ajoutant des nodes (mais aussi de la ressource)
* Il est pensé pour être tolérant à la panne (grâce à la distribution des shards). Il est interrogé en REST par une API.
* On est pas sur de l'indexation temps réel, mais Near Real Time (NRT)
* On peut utiliser Elastic pour du traitement de log mais pas que, par exemple ça peut être pour rechercher des articles dans un site d'ecommerce, faire du BI etc.
* Elasticsearch traite des documents. Un documents est un objet json. Dans le cas de l'ingestion de log, un evenement est donc un document.
* Un index est un ensemble de documents (une collection)
* Un index va être découpé en 1 ou plusieurs shards, ce qui va permettre de scaler horizontalement l'indexation et la recherche.
* Les replicas shards vont permettre de faire de la tolérance aux pannes.
* Un shard va donc etre répliqué une ou plusieurs fois sur différents nodes. Si un shard primaire disparait, un shard replica est promu primaire.
* Les shards replicas sont aussi interrogés pendant une recherche, ce qui augmente la bande passante de la recherche (pas forcément plus rapide, mais plus de recherche concurrente)
* Le shard primaire et ses replicas sont toujours séparés (pas sur le même node ...)
* Le cluster est vu green quand tous les shards primaires et replicas sont alloués.
* Le cluster est jaune quand un ou plusieurs shards replicas ne sont pas alloués.
* Le cluster est rouge quand on a des primary shards non alloués (perte de donnée)
* Un cluster est vu comme une entité unique, avec un nom
* Un cluster est composé de noeuds de différents types

*  Les masters node (minimum 3 pour faire un quorum) font du shard allocation, controle l'état du cluster, cree/delete les indexs
*  Les data nodes (les plus gourmands) contiennent les shards, donc traitent les requetent d'indexation et de recherche
*  Le ingest nodes (comme des mini logstash) qui peuvent faire un peu de preprocessing (moins poussé que les plugins logstash)
*  Les coordinating node font du load balancing (routent les requetes crud, search, aggregation)
*  Les machine learning node (nécessite x-pack, ce qui est payant)

Conseils
~~~~~~~~

* Un node ne devrait avoir qu'un seul role
* Les data nodes devraient avoir du SSD, pas plus de 32G de heap, autant de ram libre que de heap (pour gérer les requetes depuis la ram plutot que le disque), et le plus de cores cpu possibles (plus important que la clock)
* Utiliser un node de coordination si possible, pour décharger les masters et data
* Toutes les install elasticsearch doivent être pensées pour un cas en particulier, pas de solution toute faite.
* Pour qu'un node soit de coordination, il faut qu'il ait master, ingest et data à false

Kibana
------

* Kibana est composé de 4 grands plugins: Discover, Visualize, Dashboards, Management.
* Kibana injecte ses propres données dans un index elasticsearch

Discover
~~~~~~~~

* Permet d'explorer les data (ici les logs) de la stack elastic
* On créer des recherches avec des filtres, on peut les sauvegarder ...

Visualize
~~~~~~~~~

* Permet de créer ses graphes, représentation de ses recherches
* On sauvegarde ses représentations pour les mettre dans des dashboards

Dashboards
~~~~~~~~~~

* On va créer des collections de représentations
* On va pouvoir partager ces collections au travers des dashboards

Timelion
~~~~~~~~

* On utilise du time series pour la visualisation, peut etre utile pour les métriques

Dev Tools
~~~~~~~~~

* La console: une UI qui permet d'interargir avec l'api rest elasticsearch
* Le profiler, qui permet d'analyser/debuguer des requetes lentes notamment
* Le grok debugger, qui permet justement de construire ou debuguer des patterns groks (ensuitent utilisables sur beat, ou logstash)

Management
~~~~~~~~~~

* On va configurer les index patterns qu'on va aller chercher chez kibana (par exemple logstash*). Va fortement impacter le volume de données qu'on va pouvoir brasser ...
* On configure les options avancées de kibana, des dashboards etc.

X-PACK
------

* Ensemble de fonctionnalités sous licence

Security
~~~~~~~~

* AD, LDAP, SAML, SSL, audit logging etc. Une part de ces fonctionnalités est devenue gratuite en 7.x

Alerting
~~~~~~~~

* Elastalert, alerte sur certains patterns etc. On peut hooker sur un monitoring existant, ou faire du mail, slack etc.

Monitoring
~~~~~~~~~~

* On va monitorer elastic, logstash, kibana ...
* A voir si utile avec un prometheus

Reporting
~~~~~~~~~

* Création de rapports, de façon évenementielle, cronée etc.

Graph
~~~~~

* graph au sens graph de relations
* permet de decouvrir des relation dans les datas
* on pourra ensuite les visualiser dans kibana

Machine Learning
~~~~~~~~~~~~~~~~

* Permet de faire de la prediction d'événement etc.

Elastic Cloud
-------------

* Elastic se charge d'hoster elasticsearch et kibana sur aws et gcp
* Ils se chargent du scaling, de l'upgrade, ajoutent x-pack, font des backups, font du support ...

Elastic Cloud Enterprise
------------------------

* Permet de manager de façon centralisé plusieurs Elastic/Kibana/X-pack clusters
* C'est ce qu'utilise Elastic Cloud dans sa solution hostée, sauf qu'ici on est la main dessus
* On peut donc manager X clusters on premise de façon centralisée
