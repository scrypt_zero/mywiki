.. Mywiki documentation master file, created by
   sphinx-quickstart on Sun Dec 23 13:23:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Scrypt's documentation!
==================================

* reStrcturedText basics: http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html

.. toctree::
   :maxdepth: 2
   :caption: Guides:
   
   ansible
   apt
   aws
   aws-fr
   ci-cd
   cloudformation
   docker
   elasticstack
   git
   kubernetes
   kubernetes-fr
   salt
   systemd
   yum

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
