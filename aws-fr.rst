aws-fr
======

CLI
---

.. code-block:: bash

 sudo pip install awscli --ignore-installed
 complete -C `which aws_completer` aws
 aws configure --profile nomduprofil
 sudo pip install aws-shell --ignore-installed

* sur aws-shell pour définir le profile implicite

.. code-block:: bash

  .profile monprofile

IAM
---

* Identity and Access Management
* Gère les users, les groupes, les roles et les access policies
* En gros, IAM définit qui peut accéder à quelles ressources
* IAM n'est pas lié à une région (service global)
* Cependant, il est possible de façon plus fine de donner par exemple à un groupe d'utilisateur d'accéder uniquement aux instances de telle région par exemple. Mais par défaut, on applique les droits sur l'ensemble des régions.
* Tout user (comme toute ressource Amazon) est identifiée par un ARN (Amazon Ressource Name) unique.
* Par défaut, un nouveau user est crée avec aucun droit (sauf le user root crée par défaut à l'ouverture du compte aws)
* Il faut a minima supprimer la clé api du compte root, et utiliser plutot un compte IAM avec une policy "AdministratorAccess" (il vaut mieux la mettre sur un groupe)
* Le compte root devrait être protége en MFA
* Le compte root ne devrait jamais être utilisé pour des tâches quotidiennes. 
* Quand on crée un user ou un groupe IAM, on lui donne les droits minimum pour accomplir ses tâches, ni plus ni moins.
* Les users ajoutés et qui ont accès à la console, ont leur propre lien généré.
* L'account number est contenu dans le lien de l'url qu'on donne aux users, ainsi il a juste à spécifier son login/mdp
* Il vaut toujours mieux associer des policies sur des groupes plutot que des users.
* Attention, il existe 3 types de policies: 
- les managed policies définies par Amazon (exemple S3FullAccess). Toute modification du contenu de la policy (par AWS) sera répercutée sur nos entités associées.
- les custom policies, modifiées par l'administrateur du compte. Si une modification a lieu, elle impacte tous les users/groupes/roles qui l'ont associé
- les in line policies, qui sont crée de toute pièce pour uniquement concerne une entité user/groupe/role (leur modification n'est pas répercutée au dela)
* Afficher les policies en json donne plus d'infos sur leur contenu.
* Quand plusieurs policies s'appliquent à une entité, un Deny explicite prend toujours le pas sur un Allow. D'ailleurs même si ce n'est pas explicite, tout est Deny par défaut.
* Renseigner une policy de password est conseillée.
* Pour donner des droits à un service (par exemple EC2) sur un autre service, on n'attache pas directement des policies. On utilise des roles.
* Les roles sont là pour gérer des policies sur des ensembles de ressources, pour des services (voire pour des users tierce partie)
* Par exemple, on va créer un rôle pour donner le droit au service EC2 de venir écrire dans tel bucket.
* Retenir les 4 concepts: users, groups, policies, roles

.. code-block:: bash

  aws iam list-groups --profile freetiers
  aws iam list-users --profile freetiers

Infrastructure Globale
----------------------

* Les regions sont le plus au niveau d'infrastructure sur AWS. Il en existe une vingtaine réparties sur le globe, par exemple EU-WEST-3, US-EAST-1 etc.)
* On doit placer les services au plus proche des utilisateurs finaux. Il faut donc bien choisir sa région, pour minimiser les latences.
* Chaque Region possède ses propres services, et coûts. Cela peut aussi être déterminant dans le choix.
* On peut répartir des services sur plusieurs régions (pas le cas par défaut)
* Chaque Region dispose de plusieurs Availability Zone, qui lui permettent de redonder ses services (S3 par exemple).
* Une Availability Zone est composée d'un ou plusieurs data center.
* Les AZ sont reliées entre elles par des liens à forte BP et faible latence, ce qui permet d'échanger les données de redondance rapidement. Une cas de défaillance d'une AZ, le service peuvent vite redémarrer sur une autre.
* Certains services sont "globaux" (par exemple IAM), d'autres sont liés à une Region, voire à une AZ

VPC
---

* Un VPC est un réseau privé au sein d'aws, sur lequel on est administrateur. On gère quelles ressources y sont déployées, et qui y a accès.
* Par défaut, un VPC est crée en même temps que notre accompte.
* On peut déployer jusque 5 VPC par région (par défaut), ce qui correspond à la limite de gateway pour une region.
* Un VPC s'etend sur toutes les AZ de la région. Par contre, un subnet ne peut appartenir qu'à une AZ, il faut donc en créer plusieurs si on veut être redondé.
* Il contient une Internet Gateway, un route table, une NACL (network control access list), et des subnets.
* L'IG est l'équivalent du modem dans un LAN, il permet de dialoguer avec internet. La route table est l'équivalent d'une table de routage sur le routeur derrière le modem. IL définit où envoyer le trafic (par défaut internet)
* Une NACL est l'équivalent d'un firewall, il définit qui peut dialoguer avec qui, sur quel port. 
* Il existe 2 types de subnets: public et privé. On placera dans un réseau public nos instances web par exemple, et dans le privé les instances bdd, non accessibles depuis internet. Ils ne peuvent communiquer qu'avec d'autres subnets, par défaut.
* Pour relier des VPC entre eux, on peut utiliser du VPC peering. Par exemple, on pourrait avoir un VPC qui offre un service mutualisé, et plusieurs VPC consommateurs (stack elasticsearch ...)
* Dans ce cas il faut faire attention à ce qu'il n'y ait pas d'overlap de subnets.
* Un VPC est crée avec un CIDR block associé. Il faut faire attention car cela va limiter le nombre d'IP disponibles.

Internet Gateway
----------------

* On ne peut avoir qu'une IG par VPC
* L'IG est redondée / scalée automatiquement côté amazon, pas besoin de s'en occuper
* On ne peut pas détacher une IG d'un VPC qui contient des ressources, uniquement sur un VPC vide

Route Tables
------------

* Contrairement à l'IG, on peut avoir plusieurs RT par VPC
* Par défaut quand on crée un VPC, on a une RT. Elle contient une sortie de 0.0.0.0 vers internet, et un autre pour assurer le trafic local dans le cidr block du VPC
* On ne peut pas supprimer une RT qui a des dépendances (subnets associés)

NACL
----

* Les network ACL agissent comme un firewall pour le VPC
* On peut avoir une ou plusieurs NACL par VPC.
* Une NACL peut etre associé à un ou plusieurs subnets. Par contre un subnet ne peut avoir qu'une seule NACL.
* La NACL par défaut (qui autorise tout le trafic IN et OUT) est associée à tous les subnets du VPC.
* Les NACL sont stateless, c'est à dire qu'à une règle IN, il faut faire correspondre une règle OUT pour une communication bidir (exemple pour SSH ...)
* Les NACL sont évalués du plus petit ID vers le plus gros ID, puis ensuite vient la règle par défaut (*)
* Toute nouvelle NACL vierge applique du deny par défaut.
* Les NACL sont insuffisantes, il faut aussi utiliser les security groups sur les ressources pour sécuriser son VPC

Subnets
-------

* Quand on crée un VPC, il a sur chaque zone un subnet différent. Un subnet ne peut PAS être sur 2 AZ ou plus.
* Un subnet est soit public, soit privé. S'il est public, il est associé à une RT qui dispose d'une route vers l'IG. S'il est privé, il est associé à une RT qui n'a pas de route vers l'IG. Par défaut, il ne pourra donc parler qu'à un autre subnet présent dans sa RT.
* Un subnet ne peut être associé qu'à une seule RT (comme une seule NACL). Par contre on peut avoir plusieurs RT dans un VPC.
* Pour plus de facilité, on peut renommer les subnets pour bien spécifier lesquels sont privés, lesquels sont publics.
* Par défaut quand on crée un subnet, il est attaché à la main RT

AZ
--

* Les régions ont plus ou moins d'AZ, cela dépend de leur popularité / ancienneté
* Il convient de toujours redonder ses ressources sur au moins 2 AZ, pour etre fault tolerant

EC2
---

* Quelques équivalences de langage (grosse maille)

- OS: AMI (Image qui fournit un OS, plus ou moins tuné). Ca peut etre du windows, linux ... les éventuels frais de license font partie de la tarification de l'instance EC2
- RAM: Memory
- CPU: Processing Power
- HDD: EBS
- Ip addressing: internet access
- firewall: security groups

* Au niveau tarification, on a 3 modèles:

- sur demande: le plus cher, mais aussi le plus flexible, car on ne paye qu'à l'heure, et donc on peut up/down une instance et payer en conséquence
- réservée: on va réserver une instance pour 1 jusque 3 ans (on s'engage). On payera d'avance (upfront), partiellement d'avance, ou seulement à la fin. Evidemment c'est moins cher upfront
- aux enchères: l'instance "spawnera" quand le prix du marché sera inférieur ou égal à notre mise.

* On sera bien sur facturé aussi sur le type d'instance (memory optimized, compute optimized etc.), son éventuel trafic, ses licences AMI, sa localisation ...

AMI
~~~

* Amazon Machine Image: un template d'image qu'on va pouvoir déployer sur X instances
* C'est typiquement l'association d'un OS avec plus ou moins de tuning et de couches logicielles.
* Il existe 3 grands types d'image:

- les images Amazon Marketplace, qui sont payantes à l'utilisation, car elles contiennent souvent des softs à licence (os, antivirus etc.)
- les images custom à soit, où on place ce qu'on veut dedans
* les images "community", en général simplement les os sans licences les plus courants (ubuntu, debian etc.)

Instance Type
~~~~~~~~~~~~~

* Les instances sont déjà triées par familles (general purpose, compute, memory, storage optimized etc.)
* Ensuite dans chaque famille on trouve les types (small, medium etc.) qui vont déterminer le nombre de vcpu, la ram etc.
* On a aussi des options liées à EBS (EBS optimized, ce qui permet d'avoir plus de bp réseau pour dialoguer avec EBS), le type de stockage qu'elles peuvent supporter, les performances réseau ...

EBS
~~~

* Elastic Block Service, service de stockage au niveau block d'amazon, haute dispo
* Un volume EBS peut être provisionné comme "root volume" d'une instance EC2, dans ce cas il persiste meme quand l'instance meurt.
* Par contre, on peut aussi utiliser des "instance storage" comme root volume, qui eux disparaissent au shutdown de l'instance.
* On peut également provisionner des volumes EBS supplémentaires, soit à la création de l'instance, soit via l'onglet EBS, et on peut ensuite les attacher à chaud à une instance (il faut qu'elle soit dans la même région que le volume).
* Les perfs EBS sont déterminées par les IOPS (Input/output Operation per Second). Cela dépend du type de volume EBS (SSD vs HDD par exemple), mais aussi de la taille du volume, les IOPS gonflent avec sa taille.
* Plutot qu'un nombre d'IOPS, on peut parler du débit en Mo/s par exemple, car on peut mieux comparer des stockages avec des tailles d'I/O unique différent.
* Les SSD utilisent des volumes plus faibles de taille d'I/O (256ko max contre 1024ko pour un hdd) car ils le gèrent mieux.
* Un snapshot est un backup d'un volume EBS. Il faudra provisionner un nouveau volume (ou réutiliser l'actuel) pour le restaurer, il ne peut pas être attaché à une instance directement ...
