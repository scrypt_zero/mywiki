git
===

Useful commands
---------------

.. code-block:: bash

  git diff master mybranch -- myfile.txt | xcopy -selection c # compare the same file in two branches and copy to clipboard


* Manage a conflict:

.. code-block:: bash

  git status
  git checkout master
  git pull
  git checkout mabranche
  git rebase master

* A ce moment on peut avoir des conflits sur le rebase. Editer le ou les fichiers concernés pour les régler. Puis

.. code-block:: bash

  git add fichier
  git rebase -continue
  git push -f

* On va utiliser le force sur le git push vers la branche distante, car les commits ne sont plus en phase.
* Pour travailler sur une nouvelle fonction:

.. code-block:: bash

  git checkout -b add_host_itg
  git push --set-upstream origin itg

* Init d'un dépot existant

.. code-block:: bash

  git init
  git remote add origin <url_git>
  git add -vA
  git commit -m "initial import"
  git push -u origin master
